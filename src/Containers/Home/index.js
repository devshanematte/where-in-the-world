import React from 'react';
import { connect } from 'react-redux';
import { Header, Search } from '../../Components';
import Countries from './Helpers/countries';

const Home = ({
	dispatch,
	countries,
	requestStatusCountry,
	darkmode
}) => {

	return(
		<div className={darkmode ? "container-fluid" : ''}>
			<Header dispatch={dispatch} darkmode={darkmode}/>
			<Search darkmode={darkmode} dispatch={dispatch}/>
			<Countries darkmode={darkmode} dispatch={dispatch} requestStatusCountry={requestStatusCountry} countries={countries}/>
		</div>
	)

}

const mapStateToProps = (state) => {
	return {
		countries:state.countries.countries,
		requestStatusCountry:state.countries.requestStatus,
		darkmode:state.app.darkmode
	}
}

export default connect(mapStateToProps)(Home);