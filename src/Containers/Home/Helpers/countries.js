import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Api } from '../../../Services';
import { Spinner } from '../../../Components';
import Item from './item';

const Countries = ({
	countries,
	requestStatusCountry,
	dispatch,
	darkmode
}) => {

	const [page, setPage] = useState(1)

	useEffect(() => {
		Api.countries.all(dispatch)
		return
  	}, []);

	return(
		<div className="container">
			{
				requestStatusCountry ?
					<Spinner type="Oval" color="#4d5965" width={100} height={100}/>
				: countries && countries.length ?
					<div className="countries-items">
						{
							countries.slice(0, (countries.length - (countries.length-(20*page)))).map((item, index)=>{
								return <Item darkmode={darkmode} item={item} key={index} />
							})
						}

						<div className={darkmode ? "countries-more" : "countries-more countries-more-light"} >
							<span onClick={()=> setPage(Number(page+1)) }>more</span>
						</div>

					</div>
				:
					<div className="clear-data-block">
						<h3>No countries</h3>
					</div>
			
			}
		</div>
	)

}

export default Countries;