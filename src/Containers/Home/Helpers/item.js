import React from 'react';
import { ReactSVG } from 'react-svg';
import { Spinner } from '../../../Components';
import {
  Link,
  useRouteMatch
} from "react-router-dom";

const Item = ({
	item,
	darkmode
}) => {

	let match = useRouteMatch();

	return(
		<div className={darkmode ? "country-item" : "country-item country-item-light"}>
			<Link to={`${match.url}country/${item.name}`}>
				<div className="country-item-flag">
					<ReactSVG role="img" loading={() => <Spinner/>} className="country-item-flag-svg" src={item.flag} />
				</div>

				<div className="country-item-info">
					<h3>{ item.name }</h3>
					<p>Population: <span>{ item.population }</span></p>
					<p>Region: <span>{ item.region }</span></p>
					<p>Capital: <span>{ item.capital }</span></p>
				</div>
			</Link>
		</div>
	)

}

export default Item;