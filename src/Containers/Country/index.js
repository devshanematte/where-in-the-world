import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Api } from '../../Services';
import { Spinner, Header, Icons } from '../../Components';
import {
  Link,
  useRouteMatch,
  Redirect
} from "react-router-dom";
import { ReactSVG } from 'react-svg';

const Country = ({ 
	dispatch, 
	country,
	darkmode
}) => {

	let match = useRouteMatch();
	const { id } = match.params

	useEffect(()=>{

		Api.countries.getCountry(dispatch, id);
		return

	}, [])

	if(country){
		return(
			<div className={darkmode ? "container-fluid" : ''}>
				<Header dispatch={dispatch} darkmode={darkmode}/>
				<div className="container">
					
					<div className={darkmode ? "go-back-block" : "go-back-block go-back-block-light"}>
						<Link to="/">
							<div className="go-back-block-button">
								<Icons.BackIcon defaultMargin/>
								<span>Back</span>
							</div>
						</Link>
					</div>

					<div className={darkmode ? "content-page-block" : "content-page-block content-page-block-light"}>
						<div className="col-6">
							<div className="page-image">
								<ReactSVG src={country.flag} />
							</div>
						</div>
						<div className="col-3">
							<div className="page-content" style={{marginTop:50}}>
								<h1>{ country.name }</h1>

								<div className="page-content-sections">
									<section>
										<p>Native Name: <span>{ country.nativeName }</span></p>
										<p>Population: <span>{ country.population }</span></p>
										<p>Region: <span>{ country.region }</span></p>
										<p>Sub Region: <span>{ country.subregion ? country.subregion : 'No' }</span></p>
										<p>Capital: <span>{ country.capital ? country.capital : 'No' }</span></p>
									</section>
									<section>
										<p>Top Level Domain: {
											country.topLevelDomain && country.topLevelDomain.length ?
												country.topLevelDomain.map((item, index)=>{
													return <span key={index}>{ item }</span>
												})
											:
												<span>no</span>
										}</p>
										<p>Currencies: {
											country.currencies && country.currencies.length ?
												country.currencies.map((item, index)=>{
													return <span key={index}>{ item.code }</span>
												})
											:
												<span>No</span>
										}</p>
										<p>Languages: {
											country.languages && country.languages.length ?
												country.languages.map((item, index)=>{
													return <span key={index}>{ item.name }</span>
												})
											:
												<span>No</span>
										}</p>
									</section>
								</div>

								<div className="page-content-sections page-content-sections-row">
									<section>
										<p>Border Countries: {
											country && country.borders.length ?
												country.borders.map((item, index)=>{
													return <span key={index}>{item}</span>
												})
											:
												<span>No</span>
										}</p>
									</section>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		)	
	}

	return <Redirect to="/" />

}

const mapStateToProps = (state) => {
	return {
		country:state.countries.country,
		darkmode:state.app.darkmode
	}
}

export default connect(mapStateToProps)(Country);