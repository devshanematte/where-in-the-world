import React, { useState } from 'react';
import { Icons } from '../';
import { Api } from '../../Services';

const Search = ({
	dispatch,
	darkmode
}) => {

	const [statusOptions, setStatusOptions] = useState(false)
	const [search, setSearch] = useState(false)

	const openOptions = () => {
		setStatusOptions(!statusOptions)
	}

	const getRegion = (region) => {
		openOptions()
		Api.countries.region(dispatch, region)
    	return
	}

	const getSearch = (value) => {
		if(value.key === 'Enter'){
			Api.countries.search(dispatch, search)
	    	return
		}
	}

	return(
		<div className="container">
			<div className="search-block">
				
				<div className={darkmode ? "search-block-input" : "search-block-input search-block-input-light"}>
					<Icons.SearchIcon/>
					<input type="text" onChange={(value)=>{ setSearch(value.target.value) }} onKeyPress={getSearch.bind(undefined)} placeholder="Search for a country..."/>
				</div>
				<div className={darkmode ? "search-block-options" : "search-block-options search-block-options-light"} onClick={openOptions}>

					<div className={darkmode ? "search-block-option-button" : "search-block-option-button search-block-option-button-light"}>
						<p>Filter by Region</p>
						<Icons.RightIcon/>
					</div>

					<div className={statusOptions ? "search-block-option-list search-block-option-list-opened" : "search-block-option-list"}>
						<span onClick={getRegion.bind(undefined, 'Africa')}>Africa</span>
						<span onClick={getRegion.bind(undefined, 'Americas')}>America</span>
						<span onClick={getRegion.bind(undefined, 'Asia')}>Asia</span>
						<span onClick={getRegion.bind(undefined, 'Europe')}>Europe</span>
						<span onClick={getRegion.bind(undefined, 'Oceania')}>Oceania</span>
					</div>

				</div>

			</div>
		</div>
	)

}

export default Search