import React from 'react';
import { ReactSVG } from 'react-svg';
import search from './resources/search.svg';
import right from './resources/right.svg';
import moon from './resources/moon.svg';
import back from './resources/left-arrow.svg';
import moonBlack from './resources/moon-black.svg';

const SearchIcon = () => <ReactSVG className="icon-style" src={search} />;
const RightIcon = () => <ReactSVG className="icon-style rotate-45 small right" src={right} />;
const MoonIcon = () => <ReactSVG className="icon-style icon-moon" src={moon} />;
const MoonBlackIcon = () => <ReactSVG className="icon-style icon-moon" src={moonBlack} />;
const BackIcon = ({defaultMargin}) => <ReactSVG className={defaultMargin ? 'icon-style no-margin' :'icon-style'} src={back} />;

export default {
	SearchIcon,
	RightIcon,
	MoonIcon,
	BackIcon,
	MoonBlackIcon
}