import React from 'react';
import { Icons } from '../'
import '../../Services/Styles/header.css';

const Header = ({ darkmode, dispatch }) => {

	const switchDarkMode = () => {
		return dispatch({
			type:'SWITCH_DARK_MODE'
		})
	}

	return(
		<div className={darkmode ? 'header-block' : 'header-block-light'}>
			<div className="container">
				<div className="header-block-container">
					<h1 className={!darkmode ? 'dark' : ''}>Where in the world?</h1>
					<div className="switch-darkmode-button" onClick={switchDarkMode.bind(undefined)}>
						{ darkmode ? <Icons.MoonIcon/> : <Icons.MoonBlackIcon/> }
						<p className={darkmode ? 'text-dark' : 'text-light'}>Dark Mode</p>
					</div>
				</div>
			</div>
		</div>
	)

}

export default Header;