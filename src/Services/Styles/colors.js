const colors = {
	darkTheme:{
		BG: 			'hsl(230, 17%, 14%)',
		TopBGPattern: 	'hsl(232, 19%, 15%)',
		CardBG:			'#252b43',
		CardBGHover: 	'hsl(228, 28%, 20%)',
		Text: 			'hsl(228, 34%, 66%)',
		TextWhite: 		'hsl(0, 0%, 100%)',

		Toggle: 'linear gradient hsl(210, 78%, 56%) to hsl(146, 68%, 55%)'
	},
	lightTheme:{
		BG: 			'hsl(0, 0%, 100%)',
		TopBGPattern: 	'hsl(225, 100%, 98%)',
		CardBG:			'#252b43',
		CardBGHover: 	'hsl(228, 28%, 20%)',
		Text: 			'hsl(228, 12%, 44%)',
		TextDarkBlue: 	'hsl(230, 17%, 14%)',

		Toggle: 'hsl(230, 22%, 74%)'
	},

	fontSize:	'14px',
   	limeGreen:	'hsl(163, 72%, 41%)',
	brightRed: 	'hsl(356, 69%, 56%)',
	Facebook: 	'hsl(195, 100%, 50%)',
	Twitter: 	'hsl(203, 89%, 53%)',
	Instagram: 	'linear gradient hsl(37, 97%, 70%) to hsl(329, 70%, 58%)',
	YouTube: 	'hsl(348, 97%, 39%)',
	fontFamily: "'Inter', sans-serif'"
}

export default colors