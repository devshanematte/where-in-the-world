import _ from 'underscore';

const initialState = {
	countries:[],
	country:null,

	requestStatus:false
}

const countries = (state = initialState, action: any) => {
	switch (action.type){
		case 'COUNTRIES_ALL' :
			return {
				...state,
				countries:action.data
			}
		case 'GET_COUNTRY' :

			let getCountry = _.where(state.countries, {
				name:action.id
			})

			return {
				...state,
				country:getCountry && getCountry.length ? getCountry[0] : null
			}
		case 'REQUEST_STATUS_COUNTRY' :
			return {
				...state,
				requestStatus:action.status
			}
		default :
			return state
	}
}

export default countries