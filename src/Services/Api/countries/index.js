import axios from '../axios';

export default {
	all: async (dispatch) => {

		dispatch({
			type:'REQUEST_STATUS_COUNTRY',
			status:true
		})

		try{
			let { data } = await axios.create().get('/all')

			dispatch({
				type:'COUNTRIES_ALL',
				data
			})

			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})

			return
		}catch(err){
			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})
			return
		}
		
	},
	region: async (dispatch, region) => {

		dispatch({
			type:'REQUEST_STATUS_COUNTRY',
			status:true
		})

		try{
			let { data } = await axios.create().get(`/region/${region}`)

			dispatch({
				type:'COUNTRIES_ALL',
				data
			})

			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})

			return
		}catch(err){
			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})
			return
		}
		
	},
	search: async (dispatch, search) => {

		dispatch({
			type:'REQUEST_STATUS_COUNTRY',
			status:true
		})

		try{
			let { data } = await axios.create().get(`/name/${search}`)

			dispatch({
				type:'COUNTRIES_ALL',
				data
			})

			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})

			return
		}catch(err){
			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})
			return
		}
		
	},
	getCountry: async (dispatch, id) => {

		dispatch({
			type:'REQUEST_STATUS_COUNTRY',
			status:true
		})

		try{

			dispatch({
				type:'GET_COUNTRY',
				id
			})

			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})

			return
		}catch(err){
			dispatch({
				type:'REQUEST_STATUS_COUNTRY',
				status:false
			})
			return
		}
		
	},
}