import axios from 'axios';
import config from '../../Config';

const updateAxiosConfig = async () => {
	axios.defaults.baseURL = `${config.api}`;
}

updateAxiosConfig()

export {
	updateAxiosConfig
}

export default axios